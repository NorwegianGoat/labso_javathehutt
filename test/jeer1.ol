include "../src/common_types.iol"
include "../src/node_operations.iol"
include "console.iol"
include "converter.iol"
include "file.iol"
include "string_utils.iol"
include "runtime.iol"
include "security_utils.iol"

execution{ concurrent }

constants {
  MY_ADDRESS = "socket://localhost:9001",
  TTL=5
}

inputPort JeerInputPort {
Location: MY_ADDRESS
Protocol: http
Interfaces: NodeOperations
}

outputPort JeerOutputPort {
Location: "socket://localhost:9002"
Protocol: http
Interfaces: NodeOperations
}

outputPort MonitorOutput {
  Location: "socket://localhost:4242"
  Protocol: sodep
  Interfaces: NodeOperations
}

init {
  Jeer.address=MY_ADDRESS;
  MY_DOWNLOAD_PATH="Download";
  mkdir@File(MY_DOWNLOAD_PATH)(result);
  println@Console("Benvenuto, il jeer e' ora in esecuzione all'indirizzo " + Jeer.address)();
  primaConnessione
}

define primaConnessione {
  request.jeer.address=MY_ADDRESS;
  request.ttl=TTL;
  request.isFirstConnection=true;
  requestConnection@JeerOutputPort(request);
  println@Console("Ho inviato una richiesta di connessione a " + JeerOutputPort.location)();
  connectionStatus.jeerRequest.address=Jeer.address;
  connectionStatus.message=" e' connesso a ";
  connectionStatus.jeerResponse.address="socket://localhost:9002";
  sendConnectionStatus@MonitorOutput(connectionStatus)
}

define responseConnection{
  synchronized( response ) {
    if(#global.(MY_ADDRESS).jeerAdiacente<2 && isConnectionPossible.isFirstConnection){
        global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente].address=isConnectionPossible.jeer.address;
        status=true;
        println@Console("Ho risposto a " + isConnectionPossible.jeer.address + " che accetto ancora connessioni")()
      }
    }
  }

  define searchJile{
    createSecureToken@SecurityUtils()(JolellaQuery.jid);
    global.(MY_ADDRESS).listaJidRicerca[i].jid=JolellaQuery.jid;
    with(JolellaQuery){
      .fileName="Logo.png";
      .jeer.address=Jeer.address;
      .ttl= TTL
    };
    println@Console("Faccio partire la ricerca del file Upload.zip")();
    println@Console("Nella lista di richieste in attesa ho aggiunto questo jid " + JolellaQuery.jid)();
    uploadJile@JeerOutputPort(JolellaQuery)
  }

define download {
  downloadedJile.path= MY_DOWNLOAD_PATH+"/"+downloadedJile.fileName;
  toWrite.filename = downloadedJile.path;
  toWrite.format="binary";
  base64ToRaw@Converter(downloadedJile.content)(toWrite.content);
  writeFile@File(toWrite)(response);
  println@Console("Download completato")()
}

define leaveNetWork {
  jeer.address=MY_ADDRESS;
  sendDisconnection@JeerOutputPort(jeer);
  connectionStatus.jeerRequest.address=jeer.address;
  connectionStatus.message="si sta disconnettendo da ";
  connectionStatus.jeerResponse.address="socket://localhost:9002";
  sendConnectionStatus@MonitorOutput(connectionStatus);
  halt@Runtime()()
}


main
{
    [responseConnection(isConnectionPossible)(status) {
      responseConnection;
      searchJile
      }]

      [downloadJile(downloadedJile)] {
        println@Console("Un jeer mi sta mandando un Jile")();
        download;
        println@Console("Lascio il network")();
        leaveNetWork
      }

}
