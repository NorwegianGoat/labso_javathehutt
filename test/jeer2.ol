include "../src/common_types.iol"
include "../src/node_operations.iol"
include "console.iol"
include "file.iol"
include "runtime.iol"

execution{ concurrent }

constants {
  MY_ADDRESS = "socket://localhost:9002"
}

inputPort JeerInputPort {
Location: MY_ADDRESS
Protocol: http
Interfaces: NodeOperations
}

outputPort JeerOutputPort {
Location: "socket://localhost:9001"
Protocol: http
Interfaces: NodeOperations
}

outputPort MonitorOutput {
  Location: "socket://localhost:4242"
  Protocol: sodep
  Interfaces: NodeOperations
}

init {
  Jeer.address=MY_ADDRESS;
  connectionStatus.jeerRequest.address=Jeer.address;
  connectionStatus.message=" e' il primo jeer connesso a ";
  connectionStatus.jeerResponse.address="Jolella";
  sendConnectionStatus@MonitorOutput(connectionStatus);
  println@Console("Benvenuto, il jeer e' ora in esecuzione all'indirizzo " + Jeer.address)()
}

define requestConnection {
  println@Console("Ho ricevuto una richiesta di connessione da " + connectionRequest.jeer.address)();
  myConnectionRequest.jeer.address=MY_ADDRESS;
  myConnectionRequest.ttl=5;
  myConnectionRequest.isFirstConnection=true;
  println@Console("Gli invio una query chiedendogli se accetta ancora jeer ")();
  responseConnection@JeerOutputPort(myConnectionRequest)(status);
  if (status) {
    println@Console("Affermativo, salvo il suo indirizzo")();
    global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente].address=connectionRequest.jeer.address
  }
}

define upload {
  println@Console("Dispongo del file, invio")();
  toRead.format="base64";
  toRead.filename=JolellaQuery.fileName;
  readFile@File(toRead)(downloadJile.content);
  downloadJile.fileName=JolellaQuery.fileName;
  downloadJile.path=JolellaQuery.jid;
  downloadJile.hash=" ";//calcolo hash
  downloadJile@JeerOutputPort(downloadJile)
}

define leaveNetWork {
  connectionStatus.jeerRequest.address=Jeer.address;
  connectionStatus.message=" si sta disconnettendo ";
  connectionStatus.jeerResponse.address="da Jolella";
  sendConnectionStatus@MonitorOutput(connectionStatus);
  halt@Runtime()()
}

main
{
  [requestConnection(connectionRequest)]{
    requestConnection
  }

  [uploadJile(JolellaQuery)] {
    println@Console("Ho ricevuto una JolellaQuery")();
    upload
  }

  [sendDisconnection(address)]{
    println@Console("Uno dei miei jeer conosciuti si sta disconnettendo")();
    find=false;
    for(i=0,i<#global.(MY_ADDRESS).jeerAdiacente && !find,i++)
    {
      if(global.(MY_ADDRESS).jeerAdiacente[i].address==address)
      {
        println@Console("disconnetto " + global.(MY_ADDRESS).jeerAdiacente[i].address)();
        global.(MY_ADDRESS).jeerAdiacente[i].address=global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente-1].address;
        undef(global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente-1]);
        find=true;
        println@Console("L'ho rimosso dall'elenco dei miei jeer")()
      }
    };
    println@Console("Fine demo, lascio il network")();
    leaveNetWork
  }

}
