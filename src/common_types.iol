/*

Jolella File Sharing P2P Network
Copyright (C) 2019 <stefanopio.zingaro@unibo.it>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

/*
Tipo che modella il concetto di file. Il Jile si catatterizza per:
.fileName e' il nome del file
.content e' il contenuto in base 64 del file
.path e' il percorso del file sulla rete
.hash e' l'hash del file, ci consente di capire se esso e' arrivato integro
*/
type Jile: void
{
  .fileName: string
  .content: any
  .path: string
  .hash: string
}

/* Tipo che modella il concetto di Jeer. Esso si caratterizza per:
.address viene dato in input dall'utente dato da IP+PORTA
*/
type Jeer: void
{
  .address: string
}

/*
Messaggio di connessione o disconnessione alla rete.
.jeerRequest e' il nodo che sta richiedendo la connesione
.message e' il messaggio mandato al Monitor
.jeerResponse e' il nodo che risponde alla connessione

*/
type ConnectionStatusMessage: void
{
  .jeerRequest: Jeer
  .message?: string
  .jeerResponse: Jeer
}

/*
Questa e' la Query che utilizziamo quando abbiamo una richiesta da effettuare
ad un nodo.
.jile e' il file che stiamo cercando
.jeer e' il nodo che ha emesso il messaggio
.jid e' l'id del message dato da JeerAddress+NomeFile+Timestamp
.ttl il tempo di sopravvivenza della richiesta detto Time to Leave
*/
type JolellaQuery: void
{
  .fileName: string
  .jeer: Jeer
  .jid: string
  .ttl: int
}

/*
Segnala lo stato di una richiesta al monitor
.message e' una stringa che descrive lo stato della richiesta
.jeerRequest e' il nodo che aveva inviato la richiesta
.jeerResponse e' il jeer su cui e' stato trovato il file
.jid e' l'id della richiesta a cui si e' risposto
.ttl e' il numero di hopcount che sono stati necessari per trovare il file
*/
type SearchStatus: void {
  .message: string
  .jeerRequest: Jeer
  .jeerResponse: Jeer
  .jid: string
  .ttl: int
}

/*
 Creato per stabilire la connessione con i nodi
 .jeer contiene l'address del nodo che richiede la connessione
 .ttl e' il tempo di soppravvivenza della richiesta
 .isFirstConnection ritorna true o false se si tratta o meno di prima connessione
*/
type connectionRequest: void{
  .jeer: Jeer //contiene l'address del nodo che richiede la connessione
  .ttl: int
  .isFirstConnection: bool
}
