/*

Jolella File Sharing P2P Network

Il servizio jeer rappresenta un nodo della rete P2P Jolella. Ogni Jeer
e' in grado di joinare la rete, condividere file, scaricare file e
disconnettersi.
Ogni volta che un Jeer esegue una azione invia un messaggio a un servizio
monitor.

*/

include "common_types.iol"
include "string_utils.iol"
include "node_operations.iol"
include "console.iol"
include "runtime.iol"
include "exec.iol"
include "time.iol"
include "file.iol"
include "converter.iol"
include "security_utils.iol"

execution { concurrent }

constants {
  MY_ADDRESS = "socket://localhost:9001",
  MY_LOCAL_ADDRESS = "socket://localhost:9002",
  TTL=5
}

inputPort JeerInputPort {
  Location: MY_ADDRESS
  Protocol: http
  Interfaces: NodeOperations
}

inputPort CommandFromMenu {
Location: MY_LOCAL_ADDRESS
Protocol: sodep
Interfaces: MenuInterface
}

outputPort JeerOutputPort {
  Protocol:http
  Interfaces: NodeOperations
}

outputPort MonitorOutput {
  Location: "socket://localhost:4242"
  Protocol: sodep
  Interfaces: NodeOperations
}

outputPort HashFileOutputPort
{
  Interfaces: HashFileInterface
}

//embedding per la gestione e creazione dell'hash
embedded
{
  Java: "file.hash.integrity.FileIntegrity" in HashFileOutputPort
}

init {
  Jeer.address=MY_ADDRESS;
  length@StringUtils(Jeer.address)(size);
  sub=Jeer.address;
  sub.begin=9;
  sub.end=size;
  substring@StringUtils( sub )(name);
  rep=name;
  rep.regex=":";
  rep.replacement="";
  replaceAll@StringUtils(rep)(name);
  toAbsolutePath@File("")(response);
  getParentPath@File(response)(MY_DOWNLOAD_PATH);
  MY_DOWNLOAD_PATH=MY_DOWNLOAD_PATH+"/Download/"+name;
  mkdir@File(MY_DOWNLOAD_PATH)(result);
  println@Console("Benvenuto, il jeer e' ora in esecuzione all'indirizzo " + Jeer.address)()
}

/*
metodo per la gestione del download, consente al Jeer interessato di rispondere
ad una richiesta di upload da parte di un jeer che detiene il file di richiesta
*/
  define download {
    jid=downloadedJile.path;
    synchronized( jidControlList )
    {
      for(i=0,i<#global.(MY_ADDRESS).listaJidRicerca,i++)
      {
        if(global.(MY_ADDRESS).listaJidRicerca[i].jid==jid)
        {
          downloadedJile.path= MY_DOWNLOAD_PATH+"/"+downloadedJile.fileName;
          toWrite.filename = downloadedJile.path;
          toWrite.format="binary";
          base64ToRaw@Converter(downloadedJile.content)(toWrite.content);
          writeFile@File(toWrite)(response);
          hash=downloadedJile.hash;
          hashGenerator@HashFileOutputPort(downloadedJile)(downloadedJile);
          if(hash==downloadedJile.hash)
          {
            downloadedJile.content=" ";
            global.(MY_ADDRESS).jiles[#global.(MY_ADDRESS).jiles]<<downloadedJile;
            global.(MY_ADDRESS).listaJidRicerca[i]<<global.(MY_ADDRESS).listaJidRicerca[#global.(MY_ADDRESS).listaJidRicerca-1];
            undef(global.(MY_ADDRESS).listaJidRicerca[#global.(MY_ADDRESS).listaJidRicerca-1]);
            println@Console("DOWNLOAD RIUSCITO CORRETTAMENTE")()
          }
          else
          {
            println@Console("DOWNLOAD NON RIUSCITO CORRETTAMENTE")()
          }
         }
        }
      }
    }

/*
metodo che gestisce l'upload da parte di un jeer di un jile in base 64,se questi
posseggono il file lo leggono e inviano alla porta alla quale e' stata fatta
la richiesta, altrimenti inoltrano la query ai jeer adiacenti e presenti in lista
*/
define upload {
  find=false;
  with(SearchStatus) {
    .jeerResponse.address=Jeer.address;
    .message=" ricezione della query:";
    .jid=JolellaQuery.jid;
    .jeerRequest.address=JolellaQuery.jeer.address;
    .ttl=JolellaQuery.ttl
  };
  sendRequestStatus@MonitorOutput(SearchStatus);
  println@Console(global.(MY_ADDRESS)+" ricezione richiesta di ricerca query "+JolellaQuery.jid+global.(MY_ADDRESS))();
  for(i=0,i<#global.(MY_ADDRESS).jiles && !find,i++)
  {
    if(global.(MY_ADDRESS).jiles[i].fileName==JolellaQuery.fileName)
    {
      exists@File(global.(MY_ADDRESS).jiles[i].path)(exist);
      //Controllo esistenza del file richiesto
      if(exist)
      {
        scope(DimensioneFile) {
            install( default =>
              println@Console("ERRORE, dimensione file troppo grande")()
                );
                toRead.format="base64";
                toRead.filename=global.(MY_ADDRESS).jiles[i].path;
                println@Console(global.(MY_ADDRESS).jiles[i].fileName)();
                downloadJile<<global.(MY_ADDRESS).jiles[i];
                with(SearchStatus) {
                  .jeerResponse.address=Jeer.address;
                  .message=" file TROVATO query: ";
                  .jid=JolellaQuery.jid;
                  .jeerRequest.address=JolellaQuery.jeer.address;
                  .ttl=JolellaQuery.ttl
                };
                sendRequestStatus@MonitorOutput(SearchStatus);
                readFile@File(toRead)(downloadJile.content);
                downloadJile.path=JolellaQuery.jid;
                println@Console(downloadJile.fileName)();
                println@Console(downloadJile.path)();
                println@Console(downloadJile.hash)();
                JeerOutputPort.location=JolellaQuery.jeer.address;
                downloadJile@JeerOutputPort(downloadJile)
          }
      };
      find=true
    }
  };
  if(!find) {
    JolellaQuery.ttl--;
    for(i=0,i<#global.(MY_ADDRESS).jeerAdiacente && JolellaQuery.ttl>0,i++) {
      if(global.(MY_ADDRESS).jeerAdiacente[i].address!=JolellaQuery.jeer.address) {
        with(SearchStatus) {
          .jeerResponse.address=Jeer.address;
          .message=" inoltro della query";
          .jid=JolellaQuery.jid;
          .jeerRequest.address=global.(MY_ADDRESS).jeerAdiacente[i].address;
          .ttl=JolellaQuery.ttl
        };
        sendRequestStatus@MonitorOutput(SearchStatus);
        JeerOutputPort.location=global.(MY_ADDRESS).jeerAdiacente[i].address;
        uploadJile@JeerOutputPort(JolellaQuery)
      }
    }
  }
}

/*
Metodo che gestisce la richiesta di connessione da parte di un jeer
*/
define requestConnection{
    connectionStatus.jeerRequest.address=connectionRequest.jeer.address;
    connectionStatus.message="RICHIEDE connessione a";
    connectionStatus.jeerResponse.address=Jeer.address;
    sendConnectionStatus@MonitorOutput(connectionStatus);
      status=true;
      synchronized( request ){
      conosciuto=false;
      for(i=0,i<#global.(MY_ADDRESS).jeerAdiacente,i++){
        if(connectionRequest.jeer.address==global.(MY_ADDRESS).jeerAdiacente[i].address){
          conosciuto=true
        }
      };
      //se la mia lista ha meno di 5 jeer e il jeer richiedente non e' gia' presente
      if(#global.(MY_ADDRESS).jeerAdiacente<5 && !conosciuto) {
        JeerOutputPort.location=connectionRequest.jeer.address;
        with(myConnectionRequest){
          .ttl=connectionRequest.ttl.isFirstConnection;
          .isFirstConnection=true
        };
        myConnectionRequest.jeer.address=MY_ADDRESS;
        connectionStatus.jeerRequest.address=Jeer.address;
        connectionStatus.message="ACCETTA connessione da";
        connectionStatus.jeerResponse.address=connectionRequest.jeer.address;
        sendConnectionStatus@MonitorOutput(connectionStatus);
        responseConnection@JeerOutputPort(myConnectionRequest)(status);
        if (status) {
            global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente].address=connectionRequest.jeer.address;
            connectionStatus.jeerRequest.address=Jeer.address;
            connectionStatus.message="si e' CONNESSO correttamente a";
            connectionStatus.jeerResponse.address=global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente-1].address;
            sendConnectionStatus@MonitorOutput(connectionStatus)
          }
          else
          {
            connectionStatus.jeerRequest.address=Jeer.address;
            connectionStatus.message="connessione RIGETTATA da";
            connectionStatus.jeerResponse.address=connectionRequest.jeer.address;
            sendConnectionStatus@MonitorOutput(connectionStatus)
          }
        };
        /*Se il jeer e' presente nella lista oppure non c'e' spazio in lista
          la query di aggiunta alla rete viene propogata ai nodi adiacenti
        */
        if(status) {
          connectionRequest.ttl--;
          for(i=0,i<#global.(MY_ADDRESS).jeerAdiacente && connectionRequest.ttl>0,i++){
            JeerOutputPort.location=global.(MY_ADDRESS).jeerAdiacente[i].address;
            if(connectionRequest.jeer.address!=global.(MY_ADDRESS).jeerAdiacente[i].address){
              connectionStatus.jeerRequest.address=Jeer.address;
              connectionStatus.message="INOLTRA RICHIESTA di connessione a";
              connectionStatus.jeerResponse.address=global.(MY_ADDRESS).jeerAdiacente[i].address;
              sendConnectionStatus@MonitorOutput(connectionStatus);
              requestConnection@JeerOutputPort(connectionRequest)
            }
          }
        }
      }
  }

  /*
    Metodo per la gestione della risposta alla connessione da parte di un jeer
  */
  define responseConnection{
    synchronized( response ) {
      connectionStatus.jeerRequest.address=Jeer.address;
      connectionStatus.jeerResponse.address=isConnectionPossible.jeer.address;
      if(#global.(MY_ADDRESS).jeerAdiacente<2 && isConnectionPossible.isFirstConnection){
          global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente].address=isConnectionPossible.jeer.address;
          connectionStatus.message="CONNESSO alla Jolella Network all'indirizzo";
          sendConnectionStatus@MonitorOutput(connectionStatus);
        status=true
  //Se ho spazio in lista e non si tratta di una prima connessione lo aggiungo
        } else if (#global.(MY_ADDRESS).jeerAdiacente<5 && !isConnectionPossible.isFirstConnection){
          global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente].address=isConnectionPossible.jeer.address;
          connectionStatus.message="CONNESSO a";
          sendConnectionStatus@MonitorOutput(connectionStatus);
          status=true
        } else {
          status=false
        }
      }
    }

    main {
      /*
        searchJile gestisce la ricerca di un determinato file all'interno del
        network
      */
      [searchJile(name)]{
        createSecureToken@SecurityUtils()(JolellaQuery.jid);
        inserito=false;
        synchronized( jidControlList )
        {
          println@Console(#global.(MY_ADDRESS).listaJidRicerca)();
          for(i=0,i<#global.(MY_ADDRESS).listaJidRicerca,i++)
          {
            if(global.(MY_ADDRESS).listaJidRicerca[i].fileName==nome && !inserito)
            {
              global.(MY_ADDRESS).listaJidRicerca[i].jid=JolellaQuery.jid;
              inserito=true
            }
          };
          //se non trovo il file richiesto propago la query ai nodi adiacenti
          if(!trovato)
          {
            global.(MY_ADDRESS).listaJidRicerca[#global.(MY_ADDRESS).listaJidRicerca].jid=JolellaQuery.jid;
            global.(MY_ADDRESS).listaJidRicerca[#global.(MY_ADDRESS).listaJidRicerca-1].fileName=name
          }
        };
        with(JolellaQuery){
          .fileName=name;
          .jeer<<Jeer;
          .ttl= TTL
        };
        with(SearchStatus) {
          .jeerResponse.address=Jeer.address;
          .message=" inizia ricerca tramite query";
          .jid=JolellaQuery.jid;
          .jeerRequest.address=" ";
          .ttl=JolellaQuery.ttl
        };
        sendRequestStatus@MonitorOutput(SearchStatus);
        for(i=0,i<#global.(MY_ADDRESS).jeerAdiacente,i++){
          with(SearchStatus) {
            .jeerRequest.address=global.(MY_ADDRESS).jeerAdiacente[i].address;
            .message=" inoltro della query: ";
            .jid=JolellaQuery.jid;
            .jeerResponse.address=Jeer.address;
            .ttl=JolellaQuery.ttl
          };
          sendRequestStatus@MonitorOutput(SearchStatus);
          JeerOutputPort.location=global.(MY_ADDRESS).jeerAdiacente[i].address;
          uploadJile@JeerOutputPort(JolellaQuery)
        }
      }

      /*
        shareFile gestisce la condivisione del file richiesto da un jeer
        stampa il nome del file, il path di riferimento e l'hash generato
      */
      [shareFile(jile)]{
        synchronized( stampa ){
          global.(MY_ADDRESS).jiles[#global.(MY_ADDRESS).jiles]<<jile;
          hashGenerator@HashFileOutputPort(global.(MY_ADDRESS).jiles[#global.(MY_ADDRESS).jiles-1])(global.(MY_ADDRESS).jiles[#global.(MY_ADDRESS).jiles-1]);
          println@Console("INSERIMETO FILE: "+global.(MY_ADDRESS).jiles[#global.(MY_ADDRESS).jiles-1].name)();
          println@Console("PERSCORSO:       "+global.(MY_ADDRESS).jiles[#global.(MY_ADDRESS).jiles-1].path)();
          println@Console("HASH:            "+global.(MY_ADDRESS).jiles[#global.(MY_ADDRESS).jiles-1].hash)()
        }
      }

      /*
        addNode gestisce la possibilita' di aggiungere nodi della quale si viene
        a conoscenza successivamente alla prima connessione, cosi da ampliare
        la rete di un jeer
      */
      [addNode(indirizzo)]{
        connectionStatus.jeerRequest.address=Jeer.address;
        connectionStatus.message="si sta connettendo a";
        connectionStatus.jeerResponse.address=indirizzo.address;
        sendConnectionStatus@MonitorOutput(connectionStatus);
        for(i=0,i<#global.(MY_ADDRESS).jeerAdiacente,i++){
          if(indirizzo.address==global.(MY_ADDRESS).jeerAdiacente[i].address){
            conosciuto=true
          }
        };
        /*se l'indirizzo del jeer non e' gia' conosciuto e si ha meno di 5 jeer
        nella propria lista, allora invia la richiesta di aggiunta
        */
        if(#global.(MY_ADDRESS).jeerAdiacente<5&&!conosciuto) {
          request.jeer.address= MY_ADDRESS;
          request.ttl=0;
          request.isFirstConnection=false;
          JeerOutputPort.location=indirizzo.address;
          //se l'indirizzo e' errato ritorna un'errore ed un'avviso all'utente
          scope (richiestaConnessione) {
            install( default =>
              println@Console("Errore nella connessione, ti consigliamo di inserire a mano l'indirizzo di un altro nodo")();
              connectionStatus.message="tentativo di connessione FALLITO:";
              sendConnectionStatus@MonitorOutput(connectionStatus)
              );
              responseConnection@JeerOutputPort(request)(response);
              //se la risposta e' affermativa, viene aggiunto alla lista
              if (response) {
                global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente].address=indirizzo.address;
                connectionStatus.jeerRequest=Jeer;
                connectionStatus.message="si e' connesso a";
                connectionStatus.jeerResponse=global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente-1];
                sendConnectionStatus@MonitorOutput(connectionStatus)
              }
            }
            /*se la capacita' della lista e' massima allora si avvisa l'utente
            che non e' possibile inserire il jeer
            */
          } else {
            connectionStatus.jeerRequest.address=Jeer.address;
            connectionStatus.message="CAPACITA' MASSIMA di connessione: RIFIUTO:";
            connectionStatus.jeerResponse.address=indirizzo.address;
            sendConnectionStatus@MonitorOutput(connectionStatus)
          }
        }

        /*
          firstConnection gestisce la prima connesione di un jeer al network
        */
        [firstConnection(lettura)]{
          connectionStatus.jeerRequest.address=Jeer.address;
          connectionStatus.message="CONNESSIONE alla Jolella Network tramite:";
          connectionStatus.jeerResponse.address=lettura;
          sendConnectionStatus@MonitorOutput(connectionStatus);
          request.jeer.address=MY_ADDRESS;
          request.ttl=TTL;
          request.isFirstConnection=true;
          JeerOutputPort.location=lettura;
          //Se l'indirizzo inserito e' errato ritorna un'errore all'utente
          scope (richiestaConnessione) {
            install( default =>
              println@Console("Errore nella connessione, ti consigliamo di inserire a mano l'indirizzo di un altro nodo")();
              connectionStatus.message="tentativo di connessione FALLITO:";
              sendConnectionStatus@MonitorOutput(connectionStatus)
              );
              requestConnection@JeerOutputPort(request)
            }
          }

          /*
            leaveNetWork gestisce il lascito da parte di un jeer del network
          */
          [leaveNetWork()]{
            connectionStatus.jeerRequest.address=Jeer.address;
            connectionStatus.message="DISCONNESSIONE dalla Jolella Network";
            connectionStatus.jeerResponse.address="...";
            //invia al monitor la disconnessione da parte di un jeer
            sendConnectionStatus@MonitorOutput(connectionStatus);
            for(i=0,i<#global.(MY_ADDRESS).jeerAdiacente,i++)
            {
              println@Console(global.(MY_ADDRESS).jeerAdiacente[i].address)();
                JeerOutputPort.location=global.(MY_ADDRESS).jeerAdiacente[i].address;
                connectionStatus.message="INOLTRO richiesta di disconnessione a";
                connectionStatus.jeerResponse.address=global.(MY_ADDRESS).jeerAdiacente[i].address;
                sendConnectionStatus@MonitorOutput(connectionStatus);
                sendDisconnection@JeerOutputPort(Jeer)
            };
            undef( global.(MY_ADDRESS));
            connectionStatus.message="... DISCONNESSIONE dalla Jolella Network RIUSCITA";
            connectionStatus.jeerResponse.address="!";
            sendConnectionStatus@MonitorOutput(connectionStatus);
            //chiude la connessione
            halt@Runtime()()
          }

          /*
            sendDisconnection gestisce la ricezione della disconnesione da parte
            di un jeer rimuovendolo dall'elenco dei conosciuti
          */
          [sendDisconnection(jeer)]{
            find=false;
            connectionStatus.jeerRequest.address=Jeer.address;
            connectionStatus.message="ricevuta richiesta disconnesione da";
            connectionStatus.jeerResponse.address=jeer.address;
            sendConnectionStatus@MonitorOutput(connectionStatus);
            for(i=0,i<#global.(MY_ADDRESS).jeerAdiacente && !find,i++)
            {
              //se il jeer era conosciuto e quindi nella mia lista lo rimuovo
              if(global.(MY_ADDRESS).jeerAdiacente[i].address==jeer.address)
              {
                global.(MY_ADDRESS).jeerAdiacente[i]<<global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente-1];
                undef(global.(MY_ADDRESS).jeerAdiacente[#global.(MY_ADDRESS).jeerAdiacente-1]);
                connectionStatus.jeerRequest.address=jeer.address;
                connectionStatus.message="RIMOSSO dai conosciuti di";
                connectionStatus.jeerResponse.address=Jeer.address;
                sendConnectionStatus@MonitorOutput(connectionStatus);
                find=true
              }
            }
          }

          [uploadJile(JolellaQuery)] {
            upload
          }

          [downloadJile(downloadedJile)] {
            download
          }

          [requestConnection(connectionRequest)]{
            requestConnection
          }

          [responseConnection(isConnectionPossible)(status) {
            responseConnection}]
          }
