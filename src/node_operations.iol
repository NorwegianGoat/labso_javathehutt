include "common_types.iol"
/*

*/
interface NodeOperations {
RequestResponse:responseConnection(connectionRequest)(bool)
OneWay:   sendConnectionStatus(ConnectionStatusMessage),
          sendRequestStatus(SearchStatus),
          uploadJile(JolellaQuery),
          downloadJile(Jile),
          requestConnection(connectionRequest),
          sendDisconnection(Jeer)
}
/*
interfaccia che modella il concetto di menu':
*/
interface MenuInterface {
OneWay: firstConnection(string),
        leaveNetWork(void),
        addNode(Jeer),
        shareFile(Jile),
        searchJile(string)
}
/*
interfaccia che fa da tramite per la creazione dell'hash attraverso un embedding,
un Jile invia l'hash e il ricevente lo codifica.
*/
interface HashFileInterface
{
  RequestResponse:
    hashGenerator(Jile)(Jile)
}
