include "common_types.iol"
include "string_utils.iol"
include "node_operations.iol"
include "console.iol"
include "exec.iol"
include "file.iol"

execution {single}

constants {
MY_LOCAL_ADDRESS = "socket://localhost:9002"
}

outputPort CommandToJeer {
Location: MY_LOCAL_ADDRESS
Protocol:sodep
Interfaces: MenuInterface
}

main
{
  registerForInput@Console()();
  scope(jeerOff){
    install( default =>
      println@Console("Jeer OFF: avviare un Jeer con MY_LOCAL_ADDRESS:"+MY_LOCAL_ADDRESS)()
      );
      println@Console("A quale jeer desideri connetterti? ")();
      in(primaConnessione);
      firstConnection@CommandToJeer(primaConnessione)
  };
  run=true;
  scope(jeerOff){
   install( default =>
      println@Console("Jeer OFF: Chiusura menu")();
      run=false
      );
    while(run)
    {
      println@Console("Premi 1) per aggiungere un nodo conosciuto\n")();
      println@Console("Premi 2) per aggiungere file da condividere\n")();
      println@Console("Premi 3) per aggiungere directorory da condividere\n")();
      println@Console("Premi 4) per cercare un 'Jile'\n")();
      println@Console("Premi 5) per uscire\n")();
      print@Console("OPERAZIONE DESIDERATA: ")();
      in(scelta);
      if(scelta==1){
        println@Console("Che jeer desideri aggiungere alla lista dei nodi conosciuti?
        (Usa il formato: socket://xxx.xxx.xxx.xxx:portNumber)")();
        in(indirizzo.address);
        addNode@CommandToJeer(indirizzo)
      }else if(scelta==2){
        println@Console("Nome file da condividere: ")();
        in(name);
        println@Console("Path in cui e' presente il file da condividere: ")();
        in(path);
        exists@File(path)(response);
        if(response)
        {
          path=path+"/"+name;
          exists@File(path)( response );
          if(response)
          {
            with(jile){
              .fileName=name;
              .path=path;
              .content=" ";
              .hash=" "
            };
            shareFile@CommandToJeer(jile)
          }
          else
          {
            println@Console("File inesistente!!!")()
          }
        }
        else
        {
          println@Console("Path inesistente!!!")()
        }
      } else if(scelta==3){
          println@Console("Path della cartella dei file da condividere: ")();
          in(path);
          exists@File(path)( response );
          if(response){
            request.directory=path;
            request.recursive=false;
            request.dirsOnly=false;	// List only directories?
            request.info=true; // it returns also file infos. Default is false

            list@File(request)(lista);
            for(i=0,i<#lista.result,i++)
            {
              scope(errorFileHash){
                install( default =>
                  println@Console("ERRORE: problemi nell'upload del file")()
                  );
                isDirectory@File( path+"/"+lista.result[i])( response );
                if(!response){
                  jile.content= " ";
                  jile.hash= " ";
                  jile.fileName=lista.result[i];
                  jile.path=path+"/"+lista.result[i];
                  shareFile@CommandToJeer(jile)
                }
              }
           }
        }
        else
        {
          println@Console("Path inesistente!!!")()
        }
      } else if(scelta==4){
          println@Console("Jile che si desidera ricercare: ")();
          in(name);
          searchJile@CommandToJeer(name)
      } else if(scelta==5){
          println@Console("SEI SICURO DI VOLER LASCIARE LA RETE? Y/N")();
          in(scelta);
          if(scelta=="y"||scelta=="Y"){
            run=false
          }
      }
    };
    leaveNetWork@CommandToJeer()
  }
}
