/*

Jolella File Sharing P2P Network

Il servizio Monitor riceve messaggi dai Jeer e li stampa su un terminale.
Grazie al monitor e' possibile effettuare il debug della rete, vedere quali
file sono scambiati e quali nodi si sono connessi.

*/

include "common_types.iol"
include "console.iol"
include "node_operations.iol"

execution{ concurrent }

inputPort MonitorInput {
Location: "socket://localhost:4242"
Protocol: sodep
Interfaces: NodeOperations
}

init {
  println@Console("Il monitor e' in ascolto sulla porta 4242")();
  global.line = 0
}

main {

  [ sendConnectionStatus(ConnectionStatusMessage) ] {
    synchronized( token ){
      global.line++;
      println@Console(global.line + ") "+ "Il nodo " + ConnectionStatusMessage.jeerRequest.address +" "+ ConnectionStatusMessage.message +" "+ ConnectionStatusMessage.jeerResponse.address+"\n")()
    }
  }

  [ sendRequestStatus(SearchStatus) ] {
    synchronized( token ){
      global.line++;
        println@Console(global.line + ") "+ "Il nodo " + SearchStatus.jeerResponse.address+" " + SearchStatus.message+" " + SearchStatus.jid +" a "+SearchStatus.jeerRequest.address+" con TTL "+SearchStatus.ttl+"\n")()
        }
    }
  }
