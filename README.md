# LabSO_JavaTheHutt - Jolella: A P2P filesharing Network written in Jolie

Jolella is a P2P filesharing Network, written in Jolie, developed by the JavaTheHutt team.
The project was implemented during the Operating Systems Course of the University of Bologna, academic year 2018-19, in order to understand the concurrency problems on networks.

## Prerequisites

- Java(TM) SE Runtime Environment (build 1.8.0_211-b12)
- Jolie 1.7.1  (C) 2006-2019 the Jolie team

### Running a Jolella node
Running a Jolella node is pretty simple:

1. Open a new shell and start the network monitor. The monitor is a service that allows you to see the status of a peer's interactions with the network.

```sh
jolie src/monitor.ol
```

2. Open a new shell and start the menu. The menu is a necessary service to send the commands to be executed by the peer.

```sh
jolie src/menu.ol
```

3. Open a new shell and start the jeer. The jeer is the service that contains the code related to the procedures that the peer allows to execute (e.g. upload, download, share a single file, share all files contained in a directory etc etc).  

```sh
jolie src/jeer.ol
```

*N.B. By default the jeer is executed on socket://localhost:9001, if you want to share Jiles (files) on the WAN you should set your address when you start the jeer by using this command:*

```sh
jolie -C MY_ADDRESS=\"socket://YourPublicIP:ThePortYouWantToUse\" src/jeer.ol
```

*Please note that you have to configure your router properly in order to receive file requests and upload/download jiles.*

#### Notes
In this project we use a specific service to calculate the hash of the file, you can find it with all the documentation at: https://github.com/lorenzo-formato/Jolie_Hash_Calculator

## The JavaTheHutt Team - Authors

The JavaTheHutt team is composed by students of bachelor degree in Computer Science for Management, these are our repositories:

- **Davide Dozza** : [@DavideDozza] (https://gitlab.com/DavideDozza)
- **Lorenzo Formato** : [@lorenzo-formato] (https://gitlab.com/lorenzo-formato)
- **Maicol Giovannini** : [@Maicol98] (https://gitlab.com/Maicol98)
- **Matteo Tamari** : [@matteotamari] (https://gitlab.com/matteotamari)
- **Riccardo Mioli** : [@NorwegianGoat] (https://gitlab.com/NorwegianGoat)


## License

This project is licensed under the GNU v.3 License - see the [LICENSE](LICENSE)
file for details
